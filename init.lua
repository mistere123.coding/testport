testport = {}
local storage = minetest.get_mod_storage()
testport.pos_1 = minetest.deserialize(storage:get_string("1")) or {0,0,0}
testport.pos_2 = minetest.deserialize(storage:get_string("2")) or {10,10,10}



minetest.register_entity("testport:att",{
    initial_properties = {
        physical = false,
        use_texture_alpha = true,        
        collisionbox = {-0.01, -0.01, -0.01, 0.01, 0.01, 0.01},
        visual = "sprite",
        textures = {"blank.png"},
        visual_size = {x = 0.01, y = 0.01, z = 0.01},
        static_save = false,
    },
    on_step = function(self, dtime, moveresult)
        
        self._timer = self._timer + dtime
        if self._timer > 300 then
            self.object:remove()
        end
    
    end,

    _timer = 0,

})



minetest.register_chatcommand("testport_1", {
    privs = {
        server = true,
    },
    func = function(name, param)
        testport.pos_1 = minetest.get_player_by_name(name):get_pos()
        storage:set_string("1",minetest.serialize(testport.pos_1))
        return true, "Testport Pos 1 set to your position!"
    end,
})

minetest.register_chatcommand("testport_2", {
    privs = {
        server = true,
    },
    func = function(name, param)
        testport.pos_2 = minetest.get_player_by_name(name):get_pos()
        storage:set_string("2",minetest.serialize(testport.pos_2))
        return true, "Testport Pos 2 set to your position!"
    end,
})



minetest.register_chatcommand("testport_attach", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if not player then return end
        local pos = player:get_pos()
        local obj = minetest.add_entity(pos,"testport:att")
        player:set_attach(obj)
               
        return true, "you are now attached!"
    end,
})



minetest.register_chatcommand("testport_go_1", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if not player then return end
        player:set_detach()
        player:set_properties({visual_size = 3})
        player:set_pos(testport.pos_1)

        return true, "Detached you, changed your properties, and Attempted to teleport you to position 1!"
    end,
})



minetest.register_chatcommand("testport_go_2", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if not player then return end
        player:set_detach()
        player:set_properties({visual_size = .5})
        player:set_pos(testport.pos_2)

        return true, "Detached you, changed your properties, and Attempted to teleport you to position 2!"
    end,
})




minetest.register_chatcommand("testport_reset_properties", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if not player then return end
        player:set_detach()
        player:set_properties({visual_size = 1})


        return true, "Detached you and reset your properties!"
    end,
})

